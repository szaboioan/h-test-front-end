import React from "react"
import ReactDOM from "react-dom"
import "./index.css"
import App from "./App"
import reportWebVitals from "./reportWebVitals"
import {BrowserRouter} from "react-router-dom"

const user = {
    username: "user",
    roles: ["user"],
    rights: ["can_view_articles"]
}

const admin = {
    username: "admin",
    roles: ["user", "admin"],
    rights: ["can_view_articles", "can_view_users"]
}

ReactDOM.render(
    <BrowserRouter>
        <App user={user}/>
    </BrowserRouter>,
    document.querySelector("#root")
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
