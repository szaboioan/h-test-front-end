import React from "react"
import {Card} from "react-bootstrap";

export default function ProductOverview(props) {
    const {product} = props
    return (
        <div id="product-overview">
            <Card style={{width: '18rem'}}>
                <Card.Body>
                    <Card.Title>{product.label}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">Ean Number {product.ean}</Card.Subtitle>
                    <Card.Text>
                        {product.description}
                    </Card.Text>
                    <Card.Subtitle>Price:   {product.price}</Card.Subtitle>
                </Card.Body>
            </Card>
        </div>
    );
}