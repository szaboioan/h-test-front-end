import React from "react"
import {Table} from "react-bootstrap"
import {Link} from "react-router-dom"
import {MdKeyboardArrowRight} from "react-icons/all";

export default function ProductsList(props) {
    const {products} = props
    return (
        <div id="products-list">
            <Table striped bordered hover className="w-70">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Label</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {products.map((product, key) => (
                    <tr key={key}>
                        <td>{product.id}</td>
                        <td>{product.label}</td>
                        <td>{product.description}</td>
                        <td>{product.price}</td>
                        <td>
                            <Link to={"/products/" + product.id}><MdKeyboardArrowRight/></Link>
                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </div>
    );
}