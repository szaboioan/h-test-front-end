import React from "react"
import {Link} from "react-router-dom";

export default function HomeMenu(props) {
    const {active} = props
    return (
        <div id="home-menu">
            <Link to="/" className={active === "/" ? "active" : ""}>Home</Link>
            <Link to="/products" className={active === "/products" ? "active" : ""}>Products</Link>
        </div>
    )
}