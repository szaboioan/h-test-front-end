import {Button, ButtonGroup, Spinner} from "react-bootstrap"
import React, {useState} from "react"
import faker from "faker"

export default function ProductsGenerator() {
    const [isLoading, setIsLoading] = useState(false)
    const [error, setError] = useState("")
    const [message, setMessage] = useState("")

    function generate(total) {
        const products = []
        for (let i = 0; i < total; i++) {
            const product = {
                label: faker.commerce.productName(),
                description: faker.commerce.productDescription(),
                ean: "0049720026679",
                price: faker.datatype.number(),
            }
            products.push(product)
        }

        return products
    }

    function persist(products) {
        setIsLoading(true)
        fetch("http://localhost:8080/products/generate",
            {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json'
                },
                referrerPolicy: 'no-referrer',
                body: JSON.stringify(products)
            })
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoading(false)
                    setError("")
                    setMessage("Generated " + products.length + " products. Go to product page in order to visualize them.")
                },
                (error) => {
                    setIsLoading(false)
                    setError(error.message)
                }
            )
    }

    return (
        <>
            <ButtonGroup vertical aria-label="Basic example">
                <Button variant="secondary" onClick={e => {
                    persist(generate(10))
                }}>Generate 10</Button>
                <Button variant="secondary" onClick={e => {
                    persist(generate(15))
                }}>Generate 15</Button>
                <Button variant="secondary" onClick={e => {
                    persist(generate(20))
                }}>Generate 20</Button>
            </ButtonGroup>
            {message.length > 0 && (
                <h6>
                    {message}
                </h6>
            )}
            {error && (
                <h6>
                    Could not generate products.
                </h6>
            )}
            {isLoading && (
                <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            )}
        </>
    );
}