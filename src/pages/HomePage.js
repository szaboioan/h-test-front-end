import React from "react"
import 'bootstrap/dist/css/bootstrap.min.css'
import HomeMenu from "../components/Menu/HomeMenu";
import ProductsGenerator from "../components/ProductsGenerator";

export default function HomePage() {
    return (
        <div id="page">
            <HomeMenu active="/" />
            <ProductsGenerator/>
        </div>
    )
}