import React, {useEffect, useState} from "react"
import ProductOverview from "../components/Products/ProductOverview";
import {useParams} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import "../assets/styles/products.css"
import HomeMenu from "../components/Menu/HomeMenu";
import {Spinner} from "react-bootstrap";

export default function ProductOverviewPage() {
    const [isLoading, setIsLoading] = useState(false)
    const [product, setProduct] = useState(null)
    const [error, setError] = useState('')
    let {id} = useParams()

    useEffect(() => {
        setIsLoading(true)
        setTimeout(() => {
            fetch(`http://localhost:8080/products/${id}`)
                .then(res => res.json())
                .then(
                    (result) => {
                        setIsLoading(false)
                        setProduct(result)
                    },
                    // Note: it's important to handle errors here
                    // instead of a catch() block so that we don't swallow
                    // exceptions from actual bugs in components.
                    (error) => {
                        console.log(error)
                        setIsLoading(false)
                        setProduct(null)
                        setError(error.message)
                    }
                )
        }, 1000)

    }, [id])

    let content = ""
    if (error) {
        content = <div>Error: {error}</div>
    } else if (isLoading) {
        content = <>
            <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>  Loading
        </>
    } else if (product) {
        content = <ProductOverview product={product}/>
    }
    
    return (
        <div id="page">
            <HomeMenu active="/products"/>
            {content}
        </div>
    )
}