import React, {useEffect, useState} from "react"
import ProductsList from "../components/Products/ProductsList"
import "bootstrap/dist/css/bootstrap.min.css"
import "../assets/styles/products.css"
import HomeMenu from "../components/Menu/HomeMenu";
import {Spinner} from "react-bootstrap";

export default function ProductsPage() {
    const [isLoading, setIsLoading] = useState(false)
    const [productsList, setProductsList] = useState([])
    const [error, setError] = useState("")

    useEffect(() => {
        setIsLoading(true)
        setTimeout(() => {
            fetch("http://localhost:8080/products")
                .then(res => res.json())
                .then(
                    (result) => {
                        setIsLoading(false)
                        setProductsList(result)
                    },
                    // Note: it"s important to handle errors here
                    // instead of a catch() block so that we don"t swallow
                    // exceptions from actual bugs in components.
                    (error) => {
                        setIsLoading(false)
                        setProductsList([])
                        setError(error.message)
                    }
                )
        }, 1000)

    }, [])

    let content = ""
    if (error) {
        content = <div>Error: {error}</div>
    } else if (isLoading) {
        content = (
            <>
                <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner> Loading
            </>
        )
    } else {
        content = <ProductsList products={productsList}/>
    }

    return (
        <div id="page">
            <HomeMenu active="/products"/>
            {content}
        </div>
    )
}