import React from "react"
import {Route, Switch} from "react-router-dom"
import ProductsPage from "./pages/ProductsPage"
import ProductOverviewPage from "./pages/ProductOverviewPage"
import HomePage from "./pages/HomePage"
import "../src/assets/styles/generic.css"
import "../src/assets/styles/home-menu.css"
import RestrictedPage from "./pages/RestrictedPage"
import {hasRole} from "./services/auth"

export default function App({user}) {
    return (
        <Switch>
            <Route path="/products/:id" component={ProductOverviewPage}/>
            <Route exact path="/" component={HomePage}/>
            <Route exact path="/products" component={ProductsPage}/>
            {user && hasRole(user, ["admin"]) && <Route exact path="/restricted" component={RestrictedPage}/>}
        </Switch>
    )
}

