Description

- In project root directory run the command 
  
        docker-compose up -d.
  After the container is up you can
access the application in browser at port 3000 as specified in docker configuration.
- The application provides four pages home, products, product overview and restricted.
- On the home page the user can generate 10, 15 or 20 entries in the product table. Note that every
time you click to generate entries the product table is truncated. Generated entries can be visualized
on the products page. Faker was used to generate the products data.
- On the product page the users can visualize all entries present in the product table. Each product
has a link to the product overview page where the user can access more info about the selected
product.
- On the product overview page the user can see more info about the selected product. 
- The restricted page is not completed. Here the user will see a blank page is user is passed to the
App component in the index.js file. If admin is passed then on the page will be displayed an h1
element.
This basic and minimal authorization is realized in services/auth.js, index.js and App.js files.