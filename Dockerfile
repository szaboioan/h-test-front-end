FROM node:10

WORKDIR /usr/src/app/haufe-front-end

COPY package*.json ./

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]